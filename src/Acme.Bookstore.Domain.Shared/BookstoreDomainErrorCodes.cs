﻿namespace Acme.Bookstore;

public static class BookstoreDomainErrorCodes
{
    /* You can add your business exception error codes here, as constants */
    public const string AuthorAlreadyExists = "Bookstore:00001";
}
