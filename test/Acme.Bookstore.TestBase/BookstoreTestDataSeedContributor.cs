﻿using Acme.Bookstore.Authors;
using Acme.Bookstore.Books;
using System;
using System.Threading.Tasks;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;

namespace Acme.Bookstore;

public class BookstoreTestDataSeedContributor : IDataSeedContributor, ITransientDependency
{
    private readonly IRepository<Book, Guid> _bookRepository;

    public BookstoreTestDataSeedContributor(IRepository<Book, Guid> bookRepository)
    {
        _bookRepository = bookRepository;
    }
    public Task SeedAsync(DataSeedContext context)
    {
        return Task.CompletedTask;
    }
}
